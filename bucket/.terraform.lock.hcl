# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.116.0"
  hashes = [
    "h1:dY3VPn4MXUbx9exJiYY35829CRQ8Xmdjz1cmyWSlZrg=",
    "zh:014156a7e3bb9fd7f1551137e9071828ad1f52416aacb00384447f5a2f7a938a",
    "zh:1a7e55ffface1a7807c901fa7a62d177405654dfa140c8909f0760473d758a46",
    "zh:37641bbc04b9e2d860647243ca2bd8f1497ffcaa0a1f3edd1bf2667f9467ab50",
    "zh:41b3bbe45d216c3542f2a522bb610141fb111c6a0bfa15c56410292362467917",
    "zh:55c5c3b6266de95a2a46afa2d4c6624e875eb05889a44347035f8bcc6da1efe5",
    "zh:6cceefd8c5ba87ff7c3c3e957cef9394a4fab0d797d57a58fd7373d0eab02de9",
    "zh:9898d480a84af73fe2362960a135ca476e423475717c199264a8fc7efe6cd6ff",
    "zh:ab8309d1986d71e1923af65c89327ec27e11eec0b3cf764e1adc14de877a5174",
    "zh:bf52e93f5b3d55ba454a236d845214479c59f3e7396e727b1628d4914d1c23ef",
    "zh:cd845874c2c616c7eeb81c7f647a06d264f68373d828d9e6e403aff385354d3c",
    "zh:d32ea9ae4be97484fa9fd31e2de41173c3cdca789906411ef3c47e083a69cfdd",
    "zh:e7b0c0e815bbe87edfe768ecef3b768cc334ff1398c914fa2a857ccf2edc23db",
    "zh:f9ee70985d2a3540e9a1354793d42b1c74c1698ce24a1fbcfdd5fe9d925358ff",
  ]
}
